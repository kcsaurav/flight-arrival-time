public class Airplane{
	private String airline, flightNumber, make, model, departure, arrival, departureTime, arrivalTime, status;

	// getter/setter for each of the variables

	public String getAirline(){				return airline;}
	public void setAirline(String n){		airline = n;}
	public String getFlightNum(){			return flightNumber;}
	public void setFlightNum(String n){		flightNumber = n;}
	public String getMake(){				return make;}
	public void setMake(String n){			make = n;}
	public String getModel(){				return model;}
	public void setModel(String n){			model = n;}
	public String getDep(){					return departure;}
	public void setDep(String n){			departure = n;}
	public String getArrival(){				return arrival;}
	public void setArrival(String n){		arrival = n;}
	public String getDepTime(){				return departureTime;}
	public void setDepTime(String n){		departureTime = n;}
	public String getArrivalTime(){			return arrivalTime;}
	public void setArrivalTime(String n){	arrivalTime = n;}
	public String getstatus(){			return status;}
	public void setstatus(String n){	status = " ";}

	//default
	public Airplane(){
		make = "";
		model = "";
	}

	//2 argument constructor
	public Airplane(String make,String model){
		this.make=make;
		this.model=model;
	}

	//4 argument constructor
	public Airplane(String make,String model,String air,String flight){
		this.make=make;
		this.model=model;
		this.airline=air;
		this.flightNumber=flight;
	}

	//compareTo method - compares this Airplane to another Airplane by using arrival date & time then by airline
	//Arrival Date is same here so it is not compared
	public int compareTo(Airplane other) {
		if ( getArrivalTime().compareTo(other.getArrivalTime()) == 0 ) 
			return getAirline().compareTo(other.getAirline());
		return getArrivalTime().compareTo(other.getArrivalTime()); 
	}

	//equal method - compare this Airplane to another object

	public boolean equals(Object other) {
		if (other == null)		return false;	// making sure other is NOT null

		if (!(other instanceof Airplane))	return false;	// making sure other is an Airplane object

		Airplane otherAirplane = (Airplane) other;
		return this.compareTo(otherAirplane) == 0;
	}

	//toString method - returns the exact flight detail

	public String toString() {
		String result = airline + " Flight Number " + flightNumber + "\n"
				+ "Aircraft: " + make + "\n"
				+ "Departure: " + departureTime + " @ " + departure + "\n"
				+ "Arrival: " + arrivalTime + " @ " + arrival + "\n"
				+ "Status: " + status + "\n";
		return result;
	}

}  