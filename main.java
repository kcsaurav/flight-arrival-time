public class main {

	final static String[][] DATA = {
			{"Air Berlin", "4504", "Boeing", "767-300", "Zurich (ZRH)", "New York, NY (JFK)", "2-26 1000", "2-26 1325"},
			{"Qatar Airways", "212", "Boeing", "767-300", "Milano (MXP)", "New York, NY (JFK)", "2-26 1000", "2-26 1330"},
			{"Jetblue Airways", "298", "Airbus", "A320", "Tampa, FL (TPA)", "New York, NY (JFK)", "2-26 0728", "2-26 0959"},                                       
			{"Air Canada", "6851", "Airbus", "A340-300", "Dakar (DKR)", "New York, NY (JFK)", "2-26 0255", "2-26 0650"},
			{"Alaska Airlines", "1219", "Boeing", "737-800", "Boston, MA (BOS)", "New York, NY (JFK)","2-26 0940", "2-26 1055"},
			{"Delta Airlines", "6182", "Canadadair", "CRJ", "Toronto (YYZ)", "New York, NY (JFK)", "2-26 0606", "2-26 0800"},
			{"American Airline", "199", "Boeing", "767-300", "Milano (MXP)", "New York, NY (JFK)", "2-26 1010", "2-26 0130"},
			{"United Airlines", "9855", "", "", "Cairo (CAI)", "New York, NY (JFK)", "2-26 1025", "2-26 0325"},
			{"Virgin America", "483", "Airbus", "A320", "San Francisco (SFO)", "New York, NY (JFK)", "2-25 2320", "2-26 0750"},
			{"Air France", "3650", "Boeing", "767-400", "Barcelona (BCN)", "New York, NY (JFK)", "2-26 1055", "2-26 0158"},
			{"Air China", "7266", "Boeing", "757-200", "Los Angeles, CA (LAX)", "New York, NY (JFK)","2-25 2159", "2-26 0616"}
	};

	public static void selectionSort(Airplane[] airplane){
		for(int i=0; i<airplane.length; i++){
			Airplane min=airplane[i];
			int minIndex=i;
			for(int j=i+1; j<airplane.length; j++){
				if(airplane[j].compareTo(min)<0){
					min=airplane[j];
					minIndex=j;                         
				}    
			}
			Airplane temp=airplane[i];
			airplane[i]=min;
			airplane[minIndex]=temp;             
		}
	}
	
	public static void main(String[] args) {

		//Create an Array of Airplanes
		Airplane [] airplane = new Airplane[11];

		System.out.print("Original Flight Details : " + "\n");

		for(int i=0; i<airplane.length; i++){          
			airplane[i] = new Airplane();
			airplane[i].setAirline(DATA[i][0]);
			airplane[i].setFlightNum(DATA[i][1]);
			airplane[i].setMake(DATA[i][3]);
			airplane[i].setDep((DATA[i][4]));
			airplane[i].setArrival(DATA[i][5]);
			airplane[i].setDepTime(DATA[i][6]);
			airplane[i].setArrivalTime(DATA[i][7]);
			airplane[i].setstatus("");
			String orig = airplane[i].toString();
			System.out.println(orig);
		}

		System.out.println();

		selectionSort(airplane);	// Sort it

		System.out.print("Updated Flight Details : " + "\n");

		for(int i=0; i<airplane.length; i++){
			airplane[i].toString(); 
			String update = airplane[i].toString();
			System.out.println(update);
		}
	}
}